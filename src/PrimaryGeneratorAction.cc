//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// 
/// \file PrimaryGeneratorAction.cc
/// \brief Implementation of the PrimaryGeneratorAction class

#include "PrimaryGeneratorAction.hh"

#include "G4RunManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4Electron.hh"
#include "G4MuonMinus.hh"
#include "G4NeutrinoE.hh"
#include "G4NeutrinoMu.hh"
#include "G4NeutrinoTau.hh"
#include "G4AntiNeutrinoE.hh"
#include "G4AntiNeutrinoMu.hh"
#include "G4AntiNeutrinoTau.hh"
#include "G4SystemOfUnits.hh"
#include "Analysis.hh"
#include <string>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// https://indico.ihep.ac.cn/event/9624/session/4/contribution/15/material/slides/0.pdf
namespace { G4Mutex myMutex = G4MUTEX_INITIALIZER; }

PrimaryGeneratorAction::PrimaryGeneratorAction()
 : G4VUserPrimaryGeneratorAction(),
   fParticleGun(nullptr),
   fElectron(nullptr),
   fPositron(nullptr),
   fMuonM(nullptr),
   fMuonP(nullptr),
   fPionM(nullptr),
   fPionP(nullptr),
   fPi0(nullptr),
   fProton(nullptr),
   fProtonB(nullptr),
   fNeutron(nullptr),
   fNeutronB(nullptr),
   fPhoton(nullptr),
   fK0(nullptr),
   fK0B(nullptr),
   fKL0(nullptr),
   fKS0(nullptr),
   fKaonM(nullptr),
   fKaonP(nullptr),
   fSigmaM(nullptr),
   fSigmaMB(nullptr),
   fSigmaP(nullptr),
   fSigmaPB(nullptr),
   fSigma0(nullptr),
   fSigma0B(nullptr),
   fLambda0(nullptr),
   fLambda0B(nullptr),
   fDP(nullptr),
   fDM(nullptr),
   fD0(nullptr),
   fD0B(nullptr),
   fDsP(nullptr),
   fDsM(nullptr),
   fAlpha(nullptr),
   fAlphaB(nullptr),
   rand_general(nullptr)
{
  G4AutoLock lock(&myMutex);
  
  G4int nofParticles = 1;
  fParticleGun = new G4ParticleGun(nofParticles);

  // default particle kinematic
  //
  //G4ParticleDefinition* particleDefinition = G4Electron::Definition();
  //G4ParticleDefinition* particleDefinition = G4MuonMinus::Definition();
  //G4ParticleDefinition* particleDefinition = G4NeutrinoE::Definition();

  auto particleTable = G4ParticleTable::GetParticleTable();
  fElectron = particleTable->FindParticle("e-");
  fPositron = particleTable->FindParticle("e+");
  fMuonM = particleTable->FindParticle("mu-");
  fMuonP = particleTable->FindParticle("mu+");
  fPionM = particleTable->FindParticle("pi-");
  fPionP = particleTable->FindParticle("pi+");
  fPi0 = particleTable->FindParticle("pi0");
  fProton = particleTable->FindParticle("proton");
  fProtonB = particleTable->FindParticle("anti_proton");
  fNeutron = particleTable->FindParticle("neutron");
  fNeutronB = particleTable->FindParticle("anti_neutron");
  fPhoton = particleTable->FindParticle("gamma");
  fK0 = particleTable->FindParticle("kaon0");
  fK0B = particleTable->FindParticle("anti_kaon0");
  fKL0 = particleTable->FindParticle("kaon0L");
  fKS0 = particleTable->FindParticle("kaon0S");
  fKaonM = particleTable->FindParticle("kaon-");
  fKaonP = particleTable->FindParticle("kaon+");
  fSigmaM = particleTable->FindParticle("sigma-");
  fSigmaMB = particleTable->FindParticle("anti_sigma-");
  fSigmaP = particleTable->FindParticle("sigma+");
  fSigmaPB = particleTable->FindParticle("anti_sigma+");
  fSigma0 = particleTable->FindParticle("sigma0");
  fSigma0B = particleTable->FindParticle("anti_sigma0");
  fLambda0 = particleTable->FindParticle("lambda");
  fLambda0B = particleTable->FindParticle("anti_lambda");
  fDP = particleTable->FindParticle("D+");
  fDM = particleTable->FindParticle("D-");
  fD0 = particleTable->FindParticle("D0");
  fD0B = particleTable->FindParticle("anti_D0");
  fDsP = particleTable->FindParticle("Ds+");
  fDsM = particleTable->FindParticle("Ds-");
  fAlpha = particleTable->FindParticle("alpha");
  fAlphaB = particleTable->FindParticle("anti_alpha");

  double prob[20] = {4.19555e+08,2.76039e+08,8.01273e+07,3.09426e+07,2.65731e+07,3.96274e+07,4.19555e+07,2.97871e+07,1.88661e+07,1.1286e+07,6.88121e+06,7.01341e+06,6.88121e+06,4.27615e+06,3.40314e+06,2.03582e+06,969226,742545,649937,649937};
  rand_general = new G4RandGeneral(prob,20);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  G4AutoLock lock(&myMutex);
  delete fParticleGun;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

// This function is called at the begining of event
void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{  
  G4AutoLock lock(&myMutex);

  G4double worldYHalfLength = 0.;
  G4double worldZHalfLength = 0.;
  auto worldLV = G4LogicalVolumeStore::GetInstance()->GetVolume("World");
  G4Box* worldBox = dynamic_cast<G4Box*>(worldLV->GetSolid());
  if ( worldBox ) {
    worldYHalfLength = worldBox->GetYHalfLength();  
    worldZHalfLength = worldBox->GetZHalfLength();  
  }
  else {
    G4ExceptionDescription msg;
    msg << "World volume of box shape not found." << G4endl;
    msg << "Perhaps you have changed geometry." << G4endl;
    msg << "The gun will be place in the center.";
    G4Exception("B4PrimaryGeneratorAction::GeneratePrimaries()",
      "MyCode0002", JustWarning, msg);
  }

  double rnd = rand_general->shoot();
  double_t energy = 0;
  if(rnd<0.05)      energy = G4RandFlat::shoot(110,310);
  else if(rnd<0.10) energy = G4RandFlat::shoot(310,510);
  else if(rnd<0.15) energy = G4RandFlat::shoot(510,710);
  else if(rnd<0.20) energy = G4RandFlat::shoot(710,910);
  else if(rnd<0.25) energy = G4RandFlat::shoot(910,1110);
  else if(rnd<0.30) energy = G4RandFlat::shoot(1110,1310);
  else if(rnd<0.35) energy = G4RandFlat::shoot(1310,1510);
  else if(rnd<0.40) energy = G4RandFlat::shoot(1510,1710);
  else if(rnd<0.45) energy = G4RandFlat::shoot(1710,1910);
  else if(rnd<0.50) energy = G4RandFlat::shoot(1910,2110);
  else if(rnd<0.55) energy = G4RandFlat::shoot(2110,2310);
  else if(rnd<0.60) energy = G4RandFlat::shoot(2310,2510);
  else if(rnd<0.65) energy = G4RandFlat::shoot(2510,2710);
  else if(rnd<0.70) energy = G4RandFlat::shoot(2710,2910);
  else if(rnd<0.75) energy = G4RandFlat::shoot(2910,3110);
  else if(rnd<0.80) energy = G4RandFlat::shoot(3110,3310);
  else if(rnd<0.85) energy = G4RandFlat::shoot(3310,3510);
  else if(rnd<0.90) energy = G4RandFlat::shoot(3510,3710);
  else if(rnd<0.95) energy = G4RandFlat::shoot(3710,3910);
  else              energy = G4RandFlat::shoot(3910,4110);

  double x_muon = G4RandFlat::shoot(-7.50,7.50);
  double y_muon = G4RandFlat::shoot(-6.25,6.25);
  
  // single particle gun:
  fParticleGun->SetParticleDefinition(fMuonM);
  //fParticleGun->SetParticleEnergy(1000.*GeV);
  fParticleGun->SetParticleEnergy(energy*GeV);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0,0,1));
  fParticleGun->SetParticlePosition(G4ThreeVector(x_muon*cm,y_muon*cm,-worldZHalfLength));
  fParticleGun->GeneratePrimaryVertex(anEvent);

  e_beam = energy; pdg_beam = 13; x_beam = x_muon; y_beam = y_muon;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
